# PNG Shenanigans

# Purpose
The objective of this project is to create a program that can read the content
of a PNG file and take out anything that doesn't belong there (mostly C code
:eyes:)
In the end, I'd like for the program to be able to verify that a PNG file
is valid, to inject C code at the end of it and to be able to extract and run
C code from a PNG file.

- [X] Detecting headers
- [X] Checking format validity
- [X] Extracting unexpected content at the end of a PNG file
- [ ] Injecting C code in a PNG file
- [ ] Splitting a PNG with C code into a plain PNG file and a .c file
- [ ] Running C code present inside of a PNG file from the program itself

# References
- PNG Specifications: http://www.libpng.org/pub/png/spec/1.2/PNG-Contents.html
