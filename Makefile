CC=gcc
CFLAGS=-Wall -Wextra -Werror -std=c99 -g

all: main debug

main: main.c
	${CC} -o main.o -c $< ${CFLAGS}
	${CC} ${CFLAGS} main.o -o $@

debug: main.c
	${CC} -DDBGFLAGS -o main.o -c $< ${CFLAGS}
	${CC} ${CFLAGS} main.o -o $@
